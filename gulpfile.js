const gulp = require("gulp");
const sass = require("gulp-sass");
const imagemin = require('gulp-imagemin');
const minify = require('gulp-js-minify');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();



gulp.task('js', function (done) {
    gulp.src('src/script/*.js')
        // .pipe(minify())
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.stream());
    done()
});


gulp.task('img', function (done) {
    gulp.src('src/imgs/**/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/imgs'))
        .pipe(browserSync.stream());
    done()
});


gulp.task('css', function (done) {
    gulp.src('src/styles/*.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(gulp.dest('dist/styles'))
        .pipe(browserSync.stream());
    done()
});


gulp.task('build', gulp.parallel('js', 'img', 'css'));
gulp.task('build-fast', gulp.parallel('js', 'css'));


const watch = () => {
    browserSync.init({server: {baseDir: './'}});
    gulp.watch(['src/script/*.js', 'src/styles/*.scss','src/imgs/**/*.*'], gulp.series('js', 'css', 'img'));
};

gulp.task('watch', gulp.series('build', watch));
